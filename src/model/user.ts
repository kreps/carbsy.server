export interface User {
    firstName: string;
    lastName: string;
    email: string;
    password: string;

    confirmed: boolean;

    gender: string;
    race: string;
    weight: number;
    height: number;
    birthdate: number;
    location: string;
}

export interface UserSession {
    token: string;
    userId: number;
}
