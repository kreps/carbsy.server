import * as express from "express";
import * as bodyParser from "body-parser";
import {Server} from "http";

let loggerMiddleware = (request: express.Request, response: express.Response, next) => {
    console.log(`${request.method} ${request.path}`);
    next();
}

const router = express.Router();
router.get('/hello', (request, response) => {
    response.send({ tere: 'Hello world!'});
});

const app = express();
app.use(loggerMiddleware);
app.use(bodyParser.json());
app.use("/api", router);

// app.post('/', (request, response) => {
//     response.send(request.body);
// });
//
// app.get('/', (request, response) => {
//     response.send({ tere: 'Hello world234!'} );
// });


let server: Server;
server = app.listen(5000);
